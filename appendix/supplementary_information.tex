\documentclass[smallextended]{svjour3}       % onecolumn (second format)
%\documentclass[twocolumn]{svjour3}          % twocolumn
%
\smartqed  % flush right qed marks, e.g. at end of proof
%
\usepackage{graphicx}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{float}
\usepackage[numbers]{natbib}
\title{Supplementary Appendix}
\author{Peter C. Jentsch, Chris T. Bauch, Madhur Anand}
%
\begin{document}

\section{Derivation of Model Equations}


\begin{equation}
j_{n+1, 1} = d J_n + I_{n-2} + F_n
\label{juveniles}
\end{equation}


New juvenile trees are created each year according to equation \ref{juveniles}. The number of juveniles of age 1 is equal to the total number of juveniles that died last year, $d J_n =  d \sum_{k = 1}^{N} j_{n,k}$, plus the number of grey snags $I_{-2}$, plus the number of burnt snags (trees that burned the previous summer) $F_n$. 

As mentioned in the main text, we use $P_n$ to be the severity of forest fire in year $n$. It is defined by an exponentially decaying sum over the unburnt forest area from previous years, so $P_n$ is not a state variable.  

\begin{equation}
  P_{n} = T - \sum_{i=1}^{n}  F_i e^{-\kappa (n - i)} 
\end{equation}

The growth of juvenile trees is defined by equation \ref{juv_growth}. A fraction $1-d$ of juveniles from class $k-1$ grows into class $k$ juveniles, minus the trees in this class that burn, proportional to $P_n$. 

\begin{equation}
  j_{n+1, k} = (1-d) j_{n, k - 1} - \frac{\alpha_1}{T}P_n j_{n,k-1}, k = 2 ... K-1 ,K
  \label{juv_growth}
  \end{equation}
  


 The number of susceptibles in the spring of year $n+1$ is equal to the number of susceptibles in the spring of year $n$, plus the number of juveniles growing into mature trees ($(1 - d) j {n, K} $). We subtract the trees that were infested in the summer of year $n$, $I_n$, the infested \textit{and} burnt trees, $\frac{\alpha_3}{T} P_n I_n$, the trees that were only burnt, $\frac{\alpha_2}{T} P_n S_n$, and the juveniles from class $K$ that would have become mature if they had not caught fire, ($\frac{\alpha_2}{T} P_n (1-d) j_{n, K}$).

\begin{equation}
S_{n+1} = S_n + (1 - d) j_{n, K} - (I_n + \frac{\alpha_3}{T} P_n I_n) - \frac{\alpha_2}{T} P_n \left( S_n + (1-d) j_{n, K}\right)
\end{equation}


\begin{equation}
I_{n+1} = r_1 I_n e^{-\beta_1 (T - S_n - (1 - d) j_{n, K} + (I_n + \frac{\alpha_2}{T} P_n I_n) +  \frac{\alpha_2}{T} P_n \left( S_n + (1-d) j_{n, K}\right) } - \frac{\alpha_3}{T} P_n I_n
\end{equation}

The model for infested trees is based on ricker-style dynamics, where $r_1$ is the reproduction rate of beetles, and the exponential term denotes the probability that each individual will find a susceptible tree. The number of infested trees burned is subtracted after reproduction, for simplicity. As in Duncan et al., we have $I_{n+1} = r_1 I_n e^{I_n + I_{n-1} + F_n + J_{n+1}}$, where the exponent is the number of current non-susceptible trees. From the conservation of tree-equivalents $T = I_n + I_{n-1} + F_{n} + J_{n+1} + S_{n+1}$, so the exponent is just $T - S_{n+1}$.

\begin{equation}    
  F_{n+1} =  P_n \left[\frac{\alpha_1}{T} \sum_{k = 1}^{K-1} j_{n,k} + \frac{\alpha_2}{T}( S_n + (1 - d)j_{n,K}) + \frac{\alpha_3}{T}I_n\right] + \sigma_F\gamma_n 
\end{equation}

The burnt snags in the spring of year $n+1$ is equal to the sum of the number of burnt juveniles ($\frac{\alpha_1}{T} P_n \sum_i^{K-1} j_{n,K}$), susceptibles ($\frac{\alpha_2}{T}P_n(Sn  + (1-d) j_{n,K}) $), and infested ($ \frac{\alpha_3}{T}I_n P_n$).

\small
\begin{subequations}
  \begin{alignat}{2}
    j_{n+1, 1} &= d J_n + I_{n-2} + F_{n-1} \protect \label{eq1}\\
    j_{n+1, k} &= (1-d) j_{n, k - 1} - \frac{\alpha_1}{T} P_n j_{n,k-1  },\hspace{0.5cm} k = 2 ... K-1 ,K  \protect \label{eq2}\\
    S_{n+1} &= S_n + (1 - d) j_{n, K} - \left(I_n + \frac{\alpha_2}{T} P_n I_n\right) - \frac{\alpha_2}{T}P_n \left( S_n + (1-d) j_{n, K}\right) - \sigma_F \gamma_n  \protect \label{eq3}\\
    I_{n+1} &= r_1 I_n e^{-\beta_1 (T - S_{n+1})} - \frac{\alpha_2}{T} P_n I_n + \sigma_I \xi_n  \protect \label{eq4}\\
    F_{n+1} &=  P_n \left[\frac{\alpha_1}{T} \sum_{k = 0}^{K-1} j_{n,k} + \frac{\alpha_2}{T}\left( S_n + (1 - d)j_{n,K}\right) + \frac{\alpha_2}{T}I_n\right] + \sigma_F\gamma_n  \protect \label{eq5}\\
    P_{n} &= T - \sum_{i=1}^{n}  F_i e^{-\kappa (n - i)} \label{eq6}
  \end{alignat}
\end{subequations}
\normalsize

The following lemma demonstrates that the model equations preserve the total tree equivalent population present in the initial conditions. 

\begin{lemma}
  Let $I_{-1} + I_{-2} + F_{0} + J_{0} + S_{0} = T$. The equation $ I_{n-1} + I_{n-2} + F_{n} + J_{n} + S_{n} = T $ is true for all $n \geq 0$ under the evolution equations \ref{eq1}-\ref{eq6}.
\end{lemma}
\begin{proof}
  First, notice that the only individuals leaving the Juvenile compartment $J_{n}$ are the surviving oldest juvenile age class $(1 - d) j_{n, K}$ and the sum of the trees burned from each juvenile age class (except the oldest), $\frac{\alpha_1}{T} P_n \sum_{k = 1}^{K-1} j_{n,k}$. The individuals entering the Juvenile compartment are the seedlings germinating in the canopy gaps created by the gray snags $I_{n-2}$ and the trees burned the previous summer, $F_n$. Therefore we have the following equation for the total number of juvenile trees $J_{n+1}$.

  \begin{equation}
    J_{n+1} = J_n - (1 - d) j_{n, K} + I_{n-2} + F_{n} - \frac{\alpha_1}{T} P_n \sum_{k = 1}^{K-1} j_{n,k} 
  \end{equation} 


  Then, proceed by induction. The base case, $I_{-1} + I_{-2} + F_{0} + J_{0} + S_{0} = T$,  is true by definition.

  For the inductive step, assume that $I_{n-1} + I_{n-2} + F_{n} + J_{n} + S_{n} = T$ is true, then we have:
  \begin{subequations}
    \begin{equation}
    I_{n-1} + I_{n-2} + F_{n} + J_{n} + S_{n} + (1 - d) j_{n, K} - (1 - d) j_{n, K} = T\\
    \end{equation}
    \begin{align}
    \implies J_{n}+ I_{n-2}+ F_{n} - (1 - d) j_{n, K} + I_{n-1} + S_{n} &+ (1 - d) j_{n, K}\nonumber\\ + F_{n+1} - \frac{\alpha_1}{T}  P_n \sum_{k = 1}^{K-1} j_{n,k} &- \frac{\alpha_2}{T}\left( S_n + (1 - d)j_{n,K}\right)P_n \nonumber\\
    & - \frac{\alpha_2}{T}I_n P_n  - \sigma_F\gamma_n  = T
    \end{align}
    where we use the definition of $F_{n+1}$. 

    \begin{align}
    \implies J_{n+1}+ I_{n-1} + S_{n} &+ (1 - d) j_{n, K}\nonumber + F_{n+1} - \frac{\alpha_2}{T}\left( S_n + (1 - d)j_{n,K}\right)P_n\\ &- \frac{\alpha_2}{T}I_n P_n  - \sigma_F\gamma_n   = T\\
    \end{align}
    by definition of $J_{n+1}$. 


    \begin{align}
      \implies J_{n+1}+ I_{n-1} + S_{n} + (1 - d) j_{n, K} &+ F_{n+1}  \nonumber \\- \frac{\alpha_2}{T}\left( S_n + (1 - d)j_{n,K}\right)P_n &- \frac{\alpha_2}{T}I_n P_n  - \sigma_F\gamma_n + I_n - I_n = T\\
    \end{align}
    \begin{equation}
      \implies J_{n+1} + S_{n+1} + F_{n+1} + I_n + I_{n-1}   = T      
    \end{equation}

    where we obtain the conservation equation for the spring of year $n+1$ by definition of $S_{n+1}$. Therefore the inductive step is true, and the model equations conserve tree equivalents. \qed
  \end{subequations}

\end{proof}

\section{Initial conditions}

The initial conditions we use in the model are defined as follows.

\begin{subequations}
  \begin{alignat}{2}
  I_0 &= 2000\\
  I_{-1} &= 1000\\
  I_{-2} &= 0\\
  S_0 &= 108000\\
  F_0 &= 1000\\
  j_{0,k} &= 0,\hspace{0.5cm}  k = 1 ... K\\
  \label{ic}
  \end{alignat}
\end{subequations}


These conditions were chosen to provide reasonable representations of the system being modeled. The model appears to be robust to the choice of initial conditions and so the observations in the main text hold for any reasonable set of initial conditions. Note that $I_{-1} + I_{-2} + F_{0} + J_{0} + S_{0} = 110000 = T$, where $T$ is the parameter that determines the total number of stems, from the main text. 


\section{Additional Figures}


\begin{figure}
    \includegraphics[width=\textwidth]{data_base_fig.pdf}
    \caption{Maximum MPB infestation size within 500 year period, under FTP with respect to $\tau$ fraction of $m$ juvenile stands cleared. a) ($\alpha_1 = 0.02$, $\alpha_2 = 0.0025$), b) ($\alpha_1 = 0.01$, $\alpha_2 = 0.006$.), c)($\alpha_1 = 0.03$, $\alpha_2 = 0.0012$.)}
    \label{beetle_pop_yearly}
  \end{figure}
  
  \begin{figure}
    \includegraphics[width=\textwidth]{data_five_year_trim_fig.pdf}
    \caption{Maximum MPB infestation size within 500 year period, under FTP with respect to $\tau$ fraction of $m$ juvenile stands cleared, conducted every \emph{five} years. a)($\alpha_1 = 0.02$, $\alpha_2 = 0.0025$), b)($\alpha_1 = 0.01$, $\alpha_2 = 0.006$.), c)($\alpha_1 = 0.03$, $\alpha_2 = 0.0012$.)}
  \end{figure}
  
  \begin{figure}
    \includegraphics[width=\textwidth]{data_yearly_burn_fig.pdf}
    \caption{Maximum MPB infestation size within 500 year period, under CBP with respect to $\tau$ fraction of $m$ juvenile stands cleared, conducted each year. a)($\alpha_1 = 0.02$, $\alpha_2 = 0.0025$), b) ($\alpha_1 = 0.01$, $\alpha_2 = 0.006$.), c) ($\alpha_1 = 0.03$, $\alpha_2 = 0.0012$.)}
  \end{figure}
  
\end{document}
