using FireBeetleModel
using Plots
pgfplotsx()
default(dpi = 300)
default(framestyle = :box)



do_simulations()
# do_plots()
# ()
# FireBeetleModel.slope_map((),:κ)
FireBeetleModel.manuscript_plots(FireBeetleModel.a1_list,FireBeetleModel.a2_list)