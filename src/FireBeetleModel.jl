module FireBeetleModel
export do_plots,do_simulations
using DataStructures
using Distributions
using Random
using Plots
using LoopVectorization
using Measures
using ThreadsX
using DataStructures
using Statistics
using AxisKeys
using Serialization
using BenchmarkTools
const Pop = 110_000

const a1_range = LinRange(0.002,0.05,25)
const a2_range = LinRange(0.002,0.015,25)
const κ_range = [0.1]
const τ_range = LinRange(0.0,0.15,15)
const m_range = collect(1:8)
const nsims = 200
const p_labels = ["base","five_year_trim","yearly_burn"]

const a1_list = [0.02,0.01,0.03]
const a2_list = [0.0025,0.008,0.012]
include("model.jl")
include("analysis.jl")
function do_simulations()
    p = FireBeetleModel.get_default_parameters()
    # @btime a1_a2_phase_plane($p,$a1_list,$a2_list)
    a1_a2_phase_plane(p,a1_list,a2_list)
    for (a1,a2) in zip(a1_list,a2_list) 
        p_a1_a2 = merge(p, (;a1, a2)) 
        control_simulations(p_a1_a2,"$(a1)_$(a2)_control")
    end
end

function a1_a2_phase_plane(p,a1_ts_parameters,a2_ts_parameters)
    folder = "a1_a2"
    rm("simulations/$folder",recursive = true, force = true)
    mkdir("simulations/$folder")
    vars = (:a1,:a2,:κ)
    fields = (a1_range,a2_range,κ_range)
    fields_ts = (a1_ts_parameters,a2_ts_parameters,κ_range)
    pp_data = FireBeetleModel.eval_parameter_space(fields,vars,p,6_000,nsims)
    pp_data_ts = FireBeetleModel.eval_parameter_space(fields_ts,vars,p,4_000, 1)


    p_high_trim = merge(p,(τ = τ_range[end],m = m_range[end]))
    p_high_trim_burn = merge(p,(controlled_burn = true,τ = τ_range[end],m = m_range[end]))
    p_high_trim_5_years = merge(p,(n_years = 5, τ = τ_range[end],m = m_range[end]))
    pp_high_trim_data = FireBeetleModel.eval_parameter_space(fields,vars,p_high_trim,6_000,nsims)
    pp_high_trim_burn_data = FireBeetleModel.eval_parameter_space(fields,vars,p_high_trim_burn,6_000,nsims)
    p_high_trim_5_years_data= FireBeetleModel.eval_parameter_space(fields,vars,p_high_trim_5_years,6_000,nsims)
    

    axes_labels = NamedTuple{vars}(fields)
    axes_labels_ts = NamedTuple{vars}(fields_ts)
    serialize(
        "simulations/$folder/a1_a2.dat",
        (KeyedArray(pp_data; axes_labels...),KeyedArray(pp_data_ts; axes_labels_ts...))
    )
    serialize(
        "simulations/$folder/a1_a2_high_trim.dat",
        (KeyedArray(pp_high_trim_data; axes_labels...),0)
    ) 
    serialize(
        "simulations/$folder/a1_a2_high_trim_burn.dat",
        (KeyedArray(pp_high_trim_burn_data; axes_labels...),0)
    )    
    serialize(
        "simulations/$folder/a1_a2_high_trim_5_years.dat",
        (KeyedArray(p_high_trim_5_years_data; axes_labels...),0)
    )
    println("DONE!")
end

function control_simulations(p,folder)
    rm("simulations/$folder",recursive = true, force = true)
    mkdir("simulations/$folder")

    vars = (:τ,:m,:κ)
    fields = (τ_range,m_range,κ_range)
    fields_ts = map(l -> l,fields)
    p_five_years = merge(p,(n_years = 5,))
    p_burn = merge(p,(controlled_burn = true,))

    p_set = [p,p_five_years,p_burn]
    map(enumerate(zip(p_labels,p_set))) do (i,(label,parameters))
        pp_data = FireBeetleModel.eval_parameter_space(fields,vars,parameters,6_000,nsims)
        pp_data_ts = FireBeetleModel.eval_parameter_space(fields_ts,vars,parameters,4_000, 1)
        axes_labels = NamedTuple{vars}(fields)
        axes_labels_ts = NamedTuple{vars}(fields_ts)
        serialize(
            "simulations/$folder/tau_age_class_$label.dat",
            (KeyedArray(pp_data; axes_labels...),KeyedArray(pp_data_ts; axes_labels_ts...))
        )
        println("DONE!")
    end
end
function add_subplot_letters!(plot_list; pos = :top)
    for (i,sp) in enumerate(plot_list)
        letter = string(Char(i+96))
        if pos == :top
            annotate!(sp,xlims(sp)[1] + 0.02*((xlims(sp)[2]  - xlims(sp)[1])),ylims(sp)[2] - 0.10*((ylims(sp)[2]  - ylims(sp)[1])), Plots.text("$letter)", :left, 14))
        elseif pos == :bottom
            annotate!(sp,xlims(sp)[1] + 0.02*((xlims(sp)[2]  - xlims(sp)[1])),ylims(sp)[1] + 0.10*((ylims(sp)[2]  - ylims(sp)[1])), Plots.text("$letter)", :left, 14))
        end
    end
end

function plot_ts(sol_array,dir)
    inner_params = 1
    iteration_dims_outer = dimnames(sol_array)[1:end-inner_params]
    iteration_dims_inner = dimnames(sol_array)[end-inner_params + 1:end]
    iteration_outer = axiskeys(sol_array)[1:end-inner_params]
    iteration_inner = axiskeys(sol_array)[end-inner_params + 1:end]
    for (i, ind) in enumerate(Iterators.product(iteration_outer...))
        # @show ind
        indtup = (; zip(iteration_dims_outer,ind)...)
        @show indtup
        p = [plot() for i = 1:4]
        for (j, inner_ind) in enumerate(Iterators.product(iteration_inner...))
            inner_indtup = (; zip(iteration_dims_inner,inner_ind)...)            
            sol = sol_array(;indtup...,inner_indtup...)
            # display(inner_indtup)
            l = length(sol.S)
            label = replace(string(inner_indtup),r"(\d+\.\d{4})\d*" =>s"\1") |> s -> replace(s, r"(|)|," => "") 
            series = [sol.j_k,sol.S,sol.I,sol.F]
            plot!(p[1],
                1:50,
                series[1];
                label = label,
                legend = false,
                xlabel = "Age",
                color = :Black
            )
            for i in 2:length(series)
                plot!(p[i],
                    1:1001,
                    series[i][end-1000:end];
                    size = (600,600), dpi = 300,
                    label = label,
                    layout = (length(series),1),
                    legend = false,
                    xlabel = "Years",
                    color = :Black
                )
            end
        end
        plot!(p[end]; legend = false, legend_position = :outerright)
        add_subplot_letters!(p)
        fname = replace(string(indtup), r" |(|)" => "") |> s -> replace(s, r"=" => "_") |> s -> replace(s, r"(\d+\.\d{5})\d*" =>s"\1")
        plt = plot(p..., layout = (4,1))
        @show(fname)
        savefig(plt,joinpath(dir,"$fname.pdf"))
    end
end
function plot_phase_planes(output,path)
    mkdir(path)
    display(path)
    varnames = dimnames(output)[1:2]
    vars = axiskeys(output)[1:2]

    map(1: length(axiskeys(output)[end])) do i
        avg_phase_plane_a1_a2 = FireBeetleModel.average_heatmaps(vars[1],vars[2],output[:,:,i],varnames...)
        savefig(avg_phase_plane_a1_a2,joinpath(path,"class_avg_$(κ_range[i]).pdf"))

        freq_phase_plane_a1_a2 =  FireBeetleModel.freq_heatmaps(vars[1],vars[2],output[:,:,i],varnames...)
        plot!(freq_phase_plane_a1_a2)
        try
            savefig(freq_phase_plane_a1_a2,joinpath(path,"class_freq_$(κ_range[i]).pdf"))
        catch 
            println("plotting err")
        end
        if !(:a1 in varnames)
            display(vars[1])
            mort_phase_plane_a1_a2 =  FireBeetleModel.mortality_heatmaps(vars[1],vars[2],output[:,:,i],varnames...)
            savefig(mort_phase_plane_a1_a2,joinpath(path,"class_mort_$(κ_range[i]).pdf"))
        end
    end
end
function plot_opt_planes(output,path)
    p1 = heatmap(map(x -> x[1][1],output))
    p2 = heatmap(map(x -> x[1][2],output))
    p3 = heatmap(map(x -> x[2]-x[3],output))
    p = plot(p1,p2,p3)
    savefig(p,path*".svg")

end
using StatsBase

function average_heatmaps_manu(vars_x,vars_y,model_sols,varname_y,varname_x)
    S_grid = map(x -> mean(x.S[end-2_000:end]),model_sols)
    I_max_grid = map(x -> x.max_list.I,model_sols)
    F_grid = map(x -> x.max_list.F,model_sols)
    p = [
        # heatmap(vars_y,vars_x,S_grid,color=cgrad(:PuBu_4)),
        heatmap(vars_y,vars_x,I_max_grid,color=cgrad(:PuBu_4)),
        # heatmap(vars_y,vars_x,F_grid,color=cgrad(:PuBu_4)),
    ]
    @show (maximum(I_max_grid),minimum(I_max_grid))
    plot(p...,layout=(1,1),
        xlabel =string(varname_x),
        margin = 4mm,
    )
end

function manuscript_plots(a1_list,a2_list)
    default_kappa = 0.1

    for p_set in p_labels
        outbreak_sizes_p = [plot() for i in 1:length(a1_list)]
        for (i,(a1,a2)) in enumerate(zip(a1_list,a2_list,))
            fname = "$(a1)_$(a2)_control"
            data_yearly,yearly_ts = deserialize(joinpath("simulations",fname,"tau_age_class_$(p_set).dat"))
            varnames = dimnames(data_yearly)[1:2]
            vars = axiskeys(data_yearly)[1:2]
            display((p_set,(a1,a2)))
            outbreak_sizes_p[i] = average_heatmaps_manu(vars[1],vars[2],data_yearly(κ = 0.1),varnames...)
        end

        add_subplot_letters!(outbreak_sizes_p)
        plot!(outbreak_sizes_p[1]; ylabel = L"\tau")
        # plot!(outbreak_sizes_p[2]; )
        plot!(outbreak_sizes_p[end]; colorbar_title = "Maximum outbreak size")

        p = plot(outbreak_sizes_p..., layout = (1,3), size = (750,250),yrotation = -45, rightmargin = 4mm)
        savefig(p,"plots/data_$(p_set)_fig.pdf")
    end


    a1_a2_data,a1_a2_data_ts = deserialize(joinpath("simulations","a1_a2","a1_a2.dat"))
    vars_x,vars_y = axiskeys(a1_a2_data)[1:2]
    I_max_grid = map(x -> x.max_list.I,a1_a2_data(κ = 0.1))
    F_max_grid = map(x -> x.max_list.F,a1_a2_data(κ = 0.1))
    I_grid = map(x -> x.freq_list.I,a1_a2_data(κ = 0.1))
    F_grid = map(x -> x.freq_list.F,a1_a2_data(κ = 0.1))

    clims = (min(minimum(I_max_grid),minimum(F_max_grid)),max(maximum(I_max_grid),maximum(F_max_grid)))
    @show clims
    p_matrix = [
        heatmap(vars_y,vars_x,I_max_grid;color=cgrad(:Spectral_9,rev = true),clims, colorbar_title = "Population (stems)") heatmap(vars_y,vars_x,F_max_grid;color=cgrad(:Spectral_9,rev = true),clims,colorbar_title ="Population (stems)");
        heatmap(vars_y,vars_x,I_grid;color=cgrad(:PuBu_4), colorbar_title =  "Period (years)") heatmap(vars_y,vars_x,F_grid;color=cgrad(:PuBu_4), colorbar_title = "Frequency")
    ]
    add_subplot_letters!(p_matrix)
    # subplot_titles = ["Largest outbreak size", "Outbreak period", "Largest wildfire season", "Wildfire period"] 
    # for (title,p) in zip(subplot_titles,p_matrix)
    #     plot!(p; title)
    # end
    plot!.(p_matrix[:,2];xlabel = L"\alpha_2")
    plot!.(p_matrix[:,1])
    
    plot!.(p_matrix[1,:];ylabel = L"\alpha_1")
    plot!.(p_matrix[2,:])
        a1_a2_plots = plot(p_matrix...,
        yrotation = -45,
        xrotation = -45,
        size = (600,400),
        rightmargin = 5mm
    )
    savefig(a1_a2_plots,"plots/a1_a2_phase.pdf")

    a1_a2_data_high_trim,_ = deserialize(joinpath("simulations","a1_a2","a1_a2_high_trim.dat"))
    a1_a2_data_high_trim_5_years,_ = deserialize(joinpath("simulations","a1_a2","a1_a2_high_trim_5_years.dat"))
    a1_a2_data_high_trim_burn,_ = deserialize(joinpath("simulations","a1_a2","a1_a2_high_trim_burn.dat"))
    trim_data_list = [a1_a2_data_high_trim,a1_a2_data_high_trim_5_years,a1_a2_data_high_trim_burn]
    loss_maps = [map(((x1,x2),)-> 100*(x1.max_list.I - x2.max_list.I)/x1.max_list.I,zip(a1_a2_data(κ = 0.1),p(κ = 0.1))) for p in trim_data_list]
    clims = (minimum(minimum.(loss_maps)),maximum(maximum.(loss_maps)))

    p_loss_list =  heatmap.(loss_maps; xlabel = L"\alpha_2",color=cgrad(:RdBu_4),colorbar = false,clims)
    plot!(p_loss_list[1]; ylabel = L"\alpha_1")
    plot!(p_loss_list[end];colorbar = true,colorbar_title = "\\% reduction in MPB outbreak size")
    
    add_subplot_letters!(p_loss_list)
    plt = plot(p_loss_list...;layout = (1,3), size = (750,350),rightmargin = -10mm)
    savefig(plt,"plots/a1_a2_trim_gain.pdf")


    susceptible_maps = [map(((x1,x2),)-> 100*(mean(x1.S[end-2_000:end]) - mean(x2.S[end-2_000:end]))/mean(x2.S[end-2_000:end]),zip(a1_a2_data(κ = 0.1),p(κ = 0.1))) for p in trim_data_list]
    clims = (minimum(minimum.(susceptible_maps)),maximum(maximum.(susceptible_maps)))
    p_loss_list =  heatmap.(susceptible_maps; xlabel = L"\alpha_2",colorbar = false,color=cgrad(:RdBu_4),clims)
    plot!(p_loss_list[1]; ylabel = L"\alpha_1")
    plot!(p_loss_list[end];colorbar = true,colorbar_title = "\\% reduction in avg. susceptible population")

    add_subplot_letters!(p_loss_list)
    plt = plot(p_loss_list...;layout = (1,3), size = (750,350),rightmargin = -10mm)
    savefig(plt,"plots/a1_a2_trim_susceptible_loss.pdf")
    
    susceptible_maps = [map(((x1,x2),)-> 100*(mean(x2.S[end-2_000:end])/Pop),zip(a1_a2_data(κ = 0.1),p(κ = 0.1))) for p in trim_data_list]
    clims = (minimum(minimum.(susceptible_maps)),maximum(maximum.(susceptible_maps)))
    p_loss_list =  heatmap.(susceptible_maps; xlabel = L"\alpha_2",colorbar = false,color=cgrad(:RdBu_4),clims)
    plot!(p_loss_list[1]; ylabel = L"\alpha_1")
    plot!(p_loss_list[end];colorbar = true,colorbar_title = "\\% reduction in avg. susceptible population")
    add_subplot_letters!(p_loss_list)
    plt = plot(p_loss_list...;layout = (1,3), size = (750,350),rightmargin = -10mm)
    savefig(plt,"plots/a1_a2_susceptible_fraction.pdf")
end

function do_plots()
    rm("plots/",recursive = true, force = true)
    mkdir("plots")

    for (root, dirs, files) in walkdir("simulations")
        for dir in dirs
            
            mkdir(joinpath("plots/",dir))
        end
        for file in files
            output,output_ts = deserialize(joinpath(root,file))
            newpath = joinpath("plots/",root[findfirst('/',root)+1:end],file[begin:findlast('.',file)-1])
            newpath_ts = joinpath("plots/",root[findfirst('/',root)+1:end],file[begin:findlast('.',file)-1]*"_ts")
            mkdir(newpath_ts)
            # if occursin("a1", file)
                plot_ts(output_ts,newpath_ts)
            # end
            plot_phase_planes(output,newpath)
        end
    end

    manuscript_plots(a1_list,a2_list)
end


end # module
