export opt_trim
using RandomNumbers.Xorshifts
using LabelledArrays
function get_default_parameters()
    ptuple = (
        K = 50,
        r1 = 1.8,
        b1 = 10.8 * 10^(-6),
        l = 1,
        a2 = 0.01,
        a1 = 0.01,
        σ_I = 20.0,
        σ_F = 20.0,
        τ = 0.0,
        κ = 0.1,
        m = 4,
        n_years = 1,
        controlled_burn = false
    )
    return ptuple
end

struct ModelSol{LA}
    S::Vector{Float64}
    I::Vector{Float64}
    F::Vector{Float64}
    j_k::Vector{Float64}
    control_mortality::Vector{Float64}
    pest_mortality::Vector{Float64}
    max_list::LA
    freq_list::LA
    I_noise::Vector{Float64}
    F_noise::Vector{Float64}
    function ModelSol(T,K)
        S = zeros(T)
        I = zeros(T)
        F = zeros(T)
        j_k = zeros(K)
        control_mortality = zeros(T)
        pest_mortality = zeros(T)

        max_list = @LVector Float64 (:S,:I,:F)
        max_list .= 0
        freq_list = @LVector Float64 (:S,:I,:F)
        freq_list .= 0
        
        I_noise = similar(S)
        F_noise = similar(S)        
        return new{typeof(max_list)}(
            S,
            I,
            F,
            j_k,
            control_mortality,
            pest_mortality,
            max_list,
            freq_list,
            I_noise,
            F_noise
        )
    end
end
const r = Xoroshiro128Star(1)
function exp_conv(i,lst,kern)
    tot = 0.0
    @avx for j=max(1,i-length(kern)):i-1
        tot += lst[j]* kern[i-j] 
    end
    return tot
end


function model!(sol, T::Int64,Sic::Array{Float64,1}, Iic::Array{Float64,1}, Fic::Array{Float64,1}, p)
    @views sol.S[1:length(Sic)] .= Sic
    @views sol.I[1:length(Iic)] .= Iic
    @views sol.F[1:length(Fic)] .= Fic
    α_1 = p.a1/Pop
    α_2 = p.a2/Pop
    α_3 = p.a2/Pop
    randn!(r,sol.I_noise)
    randn!(r,sol.F_noise)
    gamma_I = sol.I_noise .* p.σ_I
    gamma_F = sol.F_noise .* p.σ_F
    j_n = zeros(p.K) 
    j_new = zeros(p.K)
    top_ind = collect(1:p.K)
    kern = map(k -> exp(-p.κ*k),0:500)
    @inbounds for t = length(Iic):T-1
        S_n = sol.S[t]
        I_n = sol.I[t]
        F_n = sol.F[t]
        oldest_j_n = j_n[p.K]

        if !(sol.I[t-1] + sol.I[t-2] + sum(j_n) + sol.S[t] + sol.F[t] + sol.F[t-1] ≈ Pop)
            @show sol.I[t-1] + sol.I[t-2] + sum(j_n) + sol.S[t] + sol.F[t] + sol.F[t-1]
            throw(error("leaky, model unphysical!"))
        end

        Pnew = max(0,Pop - exp_conv(t,sol.F,kern))
        j_new[1] = (1 - p.l)*sum(j_n) + sol.I[t - 2] + sol.F[t-1]
        
        @avx for j = 1:p.K-1
            j_new[j+1] = j_n[j]*(p.l - α_1*Pnew)
        end

        Snew = S_n + p.l * oldest_j_n - I_n -  α_3 * I_n * Pnew -  α_2 * (S_n + p.l*oldest_j_n)*Pnew
        Inew = I_n * p.r1 * exp(-1 * p.b1 * (Pop - Snew)) -  α_3*Pnew*I_n
        Fnew = Pnew * (α_1 * (sum(j_n) - oldest_j_n) +  α_2*(S_n + p.l*oldest_j_n) + α_3*I_n)

        control_damage = 0.0
        if  t % p.n_years == 0 # p.τ > 0.0 &&
            inds = partialsortperm!(top_ind, j_new, 1:p.m ,rev=true, initialized = true)
            @avx for k in 1:p.m
                index = inds[k]
                control_damage += p.τ*j_new[index]
                j_new[index] = (1 - p.τ)*j_new[index]
            end
            if p.controlled_burn
                Fnew += control_damage
            else
                j_new[1] += control_damage
            end
            sol.control_mortality[t] = control_damage
        end

        sol.pest_mortality[t] = I_n + α_3 * I_n * Pnew 
        I_noise = clamp(gamma_I[t], -1*Snew,Inew) 
        F_noise = clamp(gamma_F[t],-1*(Snew),Fnew)

        sol.S[t+1] = Snew + F_noise  
        sol.I[t+1] = Inew - I_noise
        sol.F[t+1] = Fnew - F_noise

        if sol.S[t+1]<0 || sol.I[t+1]<0 || sol.F[t+1]<0 || any(j_new .< 0)
            @show [I_noise,S_n,Snew,I_n,F_n,p.a1,p.a2,p.κ,p.τ, p.m]
            throw(error("model unphysical!"))
        end

        j_n .= j_new
        if t == 2120
            sol.j_k .= j_n
        end
    end

    @views sol.max_list.I = maximum(sol.I[end-500:end])
    @views sol.max_list.F = maximum(sol.F[end-500:end])
    @views sol.freq_list.S = frequency(sol.S[end-3000:end])
    @views sol.freq_list.I = frequency(sol.I[end-3000:end])
    @views sol.freq_list.F = frequency(sol.F[end-3000:end])
    
end

function eval_mpb_model(T,parameters,reps::Int64)
    K = parameters.K
    maxlag = 3
    solution = ModelSol(T,K)
    Sic = vcat(zeros(maxlag - 1) , [108_000.0])
    Iic = vcat(zeros(maxlag - 2) , [1000.0,2000.0])
    Fic = vcat(zeros(maxlag - 1), [1000.0])
    mean_sol = deepcopy(solution)
    avg_fieldnames =(:S, :I, :F,:max_list,:freq_list,:control_mortality,:pest_mortality,:j_k) #fieldnames to average over
    for i in 1:reps
        for v in avg_fieldnames
            getfield(solution,v) .= 0
        end
        model!(solution, T, Sic, Iic, Fic,parameters)
        for v in avg_fieldnames
            getfield(mean_sol,v) .= (getfield(mean_sol,v) .+ getfield(solution,v))
        end
    end
    for v in fieldnames(ModelSol)
        getfield(mean_sol,v) .= getfield(mean_sol,v) ./ reps
    end
    return mean_sol
end

function eval_parameter_space(vars,fields,params, time,reps)
    dims = [1:length(x) for x in vars]
    output_tensor = Array{ModelSol}(undef,[length(x) for x in vars]...)
    iterator_array = collect(zip(Iterators.product(vars...),Iterators.product(dims...)))
    K = length(iterator_array)
    ThreadsX.map(1:K) do k
        new_params = merge(params, NamedTuple{fields}(iterator_array[k][1]))
        average_model_sol = eval_mpb_model(time, new_params, reps)
        output_tensor[iterator_array[k][2]...] = average_model_sol
    end
    return output_tensor
end

