
export load_data,plot_data,freq_heatmaps,mortality_heatmaps,average_heatmaps

function plot_data(sol)
    data =hcat(collect(values(sol.statsdict))...)
    plot(1:500,data[1:500,:], legend = false, title = permutedims(collect(keys(sol.statsdict))), layout = (length(keys(sol.statsdict)),1))
end
using FFTW
using LaTeXStrings
function frequency(ts)
    if abs(minimum(ts) - maximum(ts)) < 200
        return 1.0
    end
    return argmax(abs.(fft(ts)[2:100]))
end
using LaTeXStrings
function freq_heatmaps(vars_x,vars_y,model_sols,varname_y,varname_x)
    S_grid = map(x -> x.freq_list.S,model_sols)
    I_grid = map(x -> x.freq_list.I,model_sols)
    F_grid = map(x -> x.freq_list.F,model_sols)
    p = [
            # heatmap(vars_y,vars_x,S_grid,color=cgrad(:PuBu_4)),
            contour(vars_y,vars_x,I_grid,color=cgrad(:PuBu_4)),
            contour(vars_y,vars_x,F_grid,color=cgrad(:PuBu_4))
        ]

    # dynamic_regions_grid = ((I_grid .!= 1) .+ 2) .+ ((F_grid .!= 1) .+ 1)
    # heatmap(vars_y,vars_x,dynamic_regions_grid,size = (800,600), xlabel = L"\alpha_2",  ylabel = L"\alpha_1",color=cgrad(:PuBu_4))
    
    plot(p...,layout=(1,2),size = (900,300),
    title = ["Average period of infested population" "Average period of burnt trees"],
    xlabel = L"\alpha_2", ylabel = L"\alpha_1")

end
function mortality_heatmaps(vars_x,vars_y,model_sols,varname_y,varname_x)
    control_mortality_grid = map(x -> mean(x.control_mortality[end-2_000:end]),model_sols)
    pest_mortality_grid = map(x -> mean(x.pest_mortality[end-2_000:end]),model_sols)
    net_mortality_grid = control_mortality_grid .+ pest_mortality_grid
    # display(pest_mortality_grid)
    p = [
        heatmap(vars_y,vars_x,control_mortality_grid,color = cgrad(:PuBu_4)),
        heatmap(vars_y,vars_x,pest_mortality_grid, color = cgrad(:PuBu_4)),
        heatmap(vars_y,vars_x,net_mortality_grid, color = cgrad(:PuBu_4))
    ]
    plot(p...,layout=(3,1), size = (700,2000), title = ["Average mortality due to control" "Average mortality due to pest" "Net average mortality"],xlabel = L"m",  ylabel = L"\tau")
end
function average_heatmaps(vars_x,vars_y,model_sols,varname_y,varname_x)
    S_grid = map(x -> mean(x.S[end-2_000:end]),model_sols)
    I_max_grid = map(x -> x.max_list.I,model_sols)
    F_grid = map(x -> x.max_list.F,model_sols)
    p = [
        heatmap(vars_y,vars_x,S_grid,color=cgrad(:PuBu_4)),
        heatmap(vars_y,vars_x,I_max_grid,color=cgrad(:PuBu_4)),
        heatmap(vars_y,vars_x,F_grid,color=cgrad(:PuBu_4)),
    ]
    plot(p...,layout=(3,1),
        title = ["Average adult population" "Largest outbreak size" "Largest wildfire size"],
        xlabel =string(varname_x), ylabel = string(varname_y),
        size = (700,2000)
    )
end
using Measures
